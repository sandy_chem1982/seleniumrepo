import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeBrowserTest {
	
	public static void main(String[] args) {
		
		
		String projectPath = System.getProperty("user.dir");
		System.out.println("projectPath: "+projectPath);
			
//		<if the chrome driver path is set in env variable then we don need below step>
		System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.selenium.dev/");
		driver.close();
		
	}

}
